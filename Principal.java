package org.david.sistema;

import org.david.ui.App;

import javafx.application.Application;

public class Principal{
	public static void main(String args[]){
		Application.launch(App.class, args);
	}
}
