package org.david.manejadores;

import java.sql.SQLException;
import java.sql.ResultSet;

import javafx.beans.property.SimpleListProperty;

import org.david.db.Conexion;
import org.david.beans.Usuario;

public class ManejadorUsuario{
	//private SimpleListProperty<Usuario> listaDeUsuarios;
	private Usuario usuarioConectado;
	private Conexion conexion;

	public ManejadorUsuario(Conexion conexion){
		//listaDeUsuarios = new SimpleListProperty<Usuario>();
		this.conexion = conexion;
	}
	public boolean autenticar(String nombre, String contra){
		if(usuarioConectado!=null)
			return true;
		ResultSet resultado = conexion.ejecutarConsulta("SELECT idUsuario, nombre, apellido, contra FROM Usuario WHERE nombre='"+nombre+"' AND contra='"+contra+"'");
		try{
			if(resultado!=null){
				if(resultado.next()){
					usuarioConectado=new Usuario(resultado.getInt("idUsuario"),resultado.getString("nombre"), resultado.getString("apellido"), resultado.getString("contra"));
				//return true;
				}
			}
		}catch(SQLException sql){
			sql.printStackTrace();
		}
		//return false;
		return usuarioConectado!=null;
	}
	public void desautenticar(){
		usuarioConectado = null;
	}
	public Usuario getUsuarioAutenticado(){
		return this.usuarioConectado;
	}
}
