import org.david.db.Conexion;
import java.sql.ResultSet;
import java.sql.SQLException;


public class Prueba{
	public static void main(String args[]){
		Conexion con = new Conexion();
		con.ejecutarSentencia("INSERT INTO Usuario (nombre, apellido, contra) VALUES ('wicho', 'gutierres', 'david')");
		ResultSet resultado = con.ejecutarConsulta("SELECT nombre, apellido, contra FROM Usuario");
		if(resultado != null){
			try{
				while(resultado.next()){
				
					System.out.println("------------------------");
					System.out.println("Nombre: "+resultado.getString("nombre"));
					System.out.println("Apellido: "+resultado.getString("apellido"));
					System.out.println("Contrasenia: "+resultado.getString("contra"));
				
				}
			}catch(SQLException sq){
				sq.printStackTrace();
			}
		}
		
	}
}
