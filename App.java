package org.david.ui;

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.event.ActionEvent;
import javafx.scene.control.TabPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TextField;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TableView;
import javafx.scene.layout.GridPane;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.util.Callback;
import java.sql.SQLException;
import javafx.scene.control.Cell;
import javafx.scene.control.Button;


import java.sql.ResultSet;


import javafx.event.EventHandler;
import javafx.event.Event;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseEvent;

import org.david.manejadores.ManejadorUsuario;
import org.david.manejadores.ManejadorContacto;
import org.david.db.Conexion;
import org.david.beans.Contacto;



public class App extends Application implements EventHandler<Event>{

	private Scene primaryScene;
	
	private Conexion conexion;
	
	private Boolean strike1, strike2, strike3;
	
	private Button btn1, btn2;
	private TabPane tpPrincipal, tpSegundo;
	private Tab tbLogin, tbContactos;

	private TextField tfNombre;
	private PasswordField pfContra;
	private Label lblNombre, lblContra;
	private Conexion cnx;
	private TableView<Contacto> tvContactos;

	private int count;
	private GridPane gpLogin, gp2;

	private ManejadorUsuario mUsuario;
	private ManejadorContacto mContacto;

	public void start(Stage primaryStage){
		cnx = new Conexion();
		
		strike1 = new Boolean(false);
		strike2 = new Boolean(false);
		strike3 = new Boolean(false);
		
		mUsuario = new ManejadorUsuario(cnx);
		mContacto = new ManejadorContacto(cnx);
		
		tpPrincipal = new TabPane();
		tpSegundo = new TabPane();
		
		tbLogin = new Tab("Login");

		gpLogin = new GridPane();
		gp2 = new GridPane();
		
		tfNombre = new TextField();
		tfNombre.setPromptText("nombre de usuario");
		tfNombre.addEventHandler(KeyEvent.KEY_RELEASED, this);

		pfContra = new PasswordField();
		pfContra.setPromptText("clave de usuario");
		pfContra.addEventHandler(KeyEvent.KEY_RELEASED, this);

		lblNombre = new Label("Nombre: ");
		lblContra = new Label("Clave: ");

		gpLogin.add(lblNombre, 0,0);
		gpLogin.add(tfNombre, 1, 0);
		gpLogin.add(lblContra, 0,1);
		gpLogin.add(pfContra, 1, 1);

		btn1 = new Button("BORRAR");
		btn1.addEventHandler(ActionEvent.ACTION,this);
		btn1.addEventHandler(MouseEvent.MOUSE_CLICKED, this);
		
		btn2 = new Button("AGREGAR");
		btn2.addEventHandler(ActionEvent.ACTION,this);
		btn2.addEventHandler(MouseEvent.MOUSE_CLICKED, this);
		
		tbLogin.setContent(gpLogin);
		
		tvContactos = new TableView<Contacto>(mContacto.getListaDeContactos());

		tvContactos.setEditable(true);
		
		TableColumn<Contacto, String> columnaNombre=new TableColumn<Contacto, String>("NOMBRE");
		//setCellValueFactory Hace una lista de datos genericas, del tipo que le propongamos
		// Dependiendo de lo que esta entre comillas sera lo que obtendra de su bean
		columnaNombre.setCellValueFactory(new PropertyValueFactory<Contacto, String>("nombre"));
		
		TableColumn<Contacto, String> columnaApellido=new TableColumn<Contacto, String>("APELLIDO");
		columnaApellido.setCellValueFactory(new PropertyValueFactory<Contacto, String>("apellido"));
		TableColumn<Contacto, String> columnaTelefono=new TableColumn<Contacto, String>("TELEFONO/CELULAR");
		columnaTelefono.setCellValueFactory(new PropertyValueFactory<Contacto, String>("telefono"));

		tvContactos.getColumns().setAll(columnaNombre, columnaApellido, columnaTelefono);
		
		tvContactos.addEventHandler(MouseEvent.MOUSE_ENTERED, this);
		tvContactos.addEventHandler(MouseEvent.MOUSE_EXITED, this);
		//Veamos primero En TableColumn el metodo setCellFactory
			/*
			Buscando en TableColumn encontramos CellFactory,
			que es la encargada del manejo de las celdas, ahora 
			encontramos setCellFactory, cuyo parametro principal es un Callback
			CallBack es una interfaz por lo que se utilizara la forma de diamante
			<Parametro,Retorno> se le da el parametro con la exigencia de un retorno
			entonces en nuetro setCellFactory pide una TableColumn<S,T> y quiere devolver una TableCell<S,T>
			cavalmente en relacionados con nuestro TableColumn tenemos a TableCell que significa en español Tabla de celdas
			y de tablecell tenemos a TextField Table Cell donde encontraremos el metodo forTableColumn() el cual si notamos, nos devuelve exactamente
			lo que pide el callBack
			
			*/
		columnaTelefono.setCellFactory(TextFieldTableCell.forTableColumn());
		columnaNombre.setCellFactory(TextFieldTableCell.forTableColumn());
		columnaApellido.setCellFactory(TextFieldTableCell.forTableColumn());
		
		columnaNombre.setCellFactory(TextFieldTableCell.forTableColumn());
		
		/*setOnEditCommit basicamente altera el valor de la tabla para
			establecerlo de nuevo, requiere de parametros un EventHandler<TableColumn.CellEditEvent<S,T>>)
			Donde S puede ser el tipo de datos generico a tratar, y el otro su dato el tipo de dato, del dato
			que vamos a tratar, en este caso los String de Contacto, lo definimos con su respectivo handle
			y listo
			Solo para aclarar t en esto es un tipo de dato CellEditEvent<Contacto,String>
			por que? por que el handle necesita manejar un tipo de dato event y CellEditEvent hereda de event
			
			ahora, realizaremos nuestro algoritmo propio para cambiar los valores, solo explicare 2 partes
			que son fundamentales 1, la sentencia en SQL
			UPDATE Tabla SET fila='nuevoValor' WHERE fila='identifiador'
			y como llegamos a convertir t en un contacto,
			para empezar ahora t es un CellEditEvent<Contacto,String>, bueno, esta clase tiene la posibilidad de
			obtener la tabla (tableView) sobre la que sucede el evento, en este caso la edicion, 
			ya teniendo la tableView podemos utilizar el metodo .getItems() el cual nos devolvera un ObservableList<ConteniendoElTipoDeDatosDelTableView>
			y este estimado ObservableList, tiene posibilidad del metodo obserbableList.get(int) obteniendo el elemento de la lista en dicha posicion
			entonces obtengamos el elemento de la lista en la posicion donde se produjo el evento con t.getTablePosition(), devolviendonos un TablePosition el
			cual representa una sola fila/columna/celda dependiendo el metodo de esta clase que se use, en nuestro caso utilizaremos getRow() que cuya funcion es
			devolvernos el numero de la fila en la que se produjo el evento. 
			AHORA ¿que tenemos? TENEMOS de la lista de Items, seleccionado el Item sobre el que se produjo el evento, este item, sin nada mas, ni nada menos puede ser
			convertido en un Contacto, simplemente se le coloca (Contacto) al principio, terminando asi:
			((Contacto) t.getTableView().getItems().get(t.getTablePosition().getRow())) ahora solo le agregamos setNombre("Lo que querramos") o
			t.getNewValue, que es el nuevo valor recibido por la edicion.
		*/
		columnaNombre.setOnEditCommit(new EventHandler<CellEditEvent<Contacto, String>>() {
		@Override
		public void handle(CellEditEvent<Contacto, String> t) {
			/*	System.out.println("Esto es T "+t);
				System.out.println("t.getNewValue() "+t.getNewValue());
				System.out.println("t.getOldValue() "+t.getOldValue());
				System.out.println("Esto es T getTableView "+t.getTableView());
				System.out.println("Esto es  t.getTableView().getItems().get(t.getTablePosition().getRow()) "+ t.getTableView().getItems().get(t.getTablePosition().getRow()));
				System.out.println("Esto es  t.getTableView().getItems().get(t.getTablePosition().getRow()).getNombre() "+ t.getTableView().getItems().get(t.getTablePosition().getRow()).getNombre());
			*/
				if(t.getNewValue()!=(t.getOldValue())){
					//try{
					conexion = new Conexion();
					//Conexion conexion2 = new Conexion();
					//ResultSet resultado = conexion2.ejecutarConsulta("SELECT idContacto FROM Contactos WHERE nombre='"+t.getNewValue()+"'");
				//	System.out.println("idContacto DETECTADO "+resultado.getInt("idContacto"));
					conexion.ejecutarSentencia("UPDATE Contactos SET nombre='"+t.getNewValue()+"' WHERE nombre='"+t.getOldValue()+"'");
					((Contacto) t.getTableView().getItems().get(t.getTablePosition().getRow())).setNombre(t.getNewValue());
					if((t.getNewValue().equals("")) || (t.getNewValue().equals(" "))){
						System.out.println("Detecto como nulo 1");
						strike1=true;
						count++;
						if(strike1==true){
							if(strike2==true){
								if(strike3==true){
									eliminar(mContacto);
									strike1=false;
									strike2=false;
									strike3=false;
								}
							}
						}
					}
				/*	}catch(SQLException e){
					}*/
				}else{
				}
				
				
			}
		}
		);
		columnaApellido.setCellFactory(TextFieldTableCell.forTableColumn());
		columnaApellido.setOnEditCommit(new EventHandler<CellEditEvent<Contacto, String>>() {
			@Override
			public void handle(CellEditEvent<Contacto, String> t) {
				if(t.getNewValue()!=(t.getOldValue())){
					conexion = new Conexion();
					conexion.ejecutarSentencia("UPDATE Contactos SET apellido='"+t.getNewValue()+"' WHERE apellido='"+t.getOldValue()+"'");
					((Contacto) t.getTableView().getItems().get(t.getTablePosition().getRow())).setApellido(t.getNewValue());
					if((t.getNewValue().equals("")) || (t.getNewValue().equals(" "))){
						System.out.println("Detecto como nulo 2");
						strike2=true;
						count++;
						if(strike1==true){
							if(strike2==true){
								if(strike3==true){
									eliminar(mContacto);
									strike1=false;
									strike2=false;
									strike3=false;
								}
							}
						}
					}	
				}else{
				}
			}
		}
		);
		columnaTelefono.setCellFactory(TextFieldTableCell.forTableColumn());
		columnaTelefono.setOnEditCommit(new EventHandler<CellEditEvent<Contacto, String>>() {
			@Override
			public void handle(CellEditEvent<Contacto, String> t) {
				if(t.getNewValue()!=(t.getOldValue())){
					conexion = new Conexion();
					conexion.ejecutarSentencia("UPDATE Contactos SET telefotno='"+t.getNewValue()+"' WHERE telefotno='"+t.getOldValue()+"'");
					((Contacto) t.getTableView().getItems().get(t.getTablePosition().getRow())).setTelefono(t.getNewValue());
					if((t.getNewValue().equals("")) || (t.getNewValue().equals(" "))){
						System.out.println("Detecto como nulo 3");
						strike3=true;
						count++;
						if(strike1==true){
							if(strike2==true){
								if(strike3==true){
									eliminar(mContacto);
									strike1=false;
									strike2=false;
									strike3=false;
								}
							}
						}
					}	
				}else{
				}		
			}
		}
		);
		
		tbContactos = new Tab("Contactos");
		gp2.add(tvContactos, 0,0);
		gp2.add(btn1,1,0);
	//	gp2.add(btn2,1,1);
		tbContactos.setContent(gp2);
		
		tpPrincipal.getTabs().add(tbLogin);

		primaryScene = new Scene(tpPrincipal);
		
		primaryStage.setScene(primaryScene);

		primaryStage.show();
	}
	
	private boolean verificarDatos(){
		return !tfNombre.getText().trim().equals("") & !pfContra.getText().trim().equals("");
	}
	public void eliminar(ManejadorContacto mContacto){
		Conexion conex = new Conexion();
		conex.ejecutarSentencia("DELETE FROM Contactos WHERE nombre='"+tvContactos.getSelectionModel().getSelectedItem().getNombre()+"'");
		Contacto cont = tvContactos.getSelectionModel().getSelectedItem();
		int posicion = mContacto.getListaDeContactos().indexOf(cont);
		mContacto.getListaDeContactos().remove(posicion);
	}
	
	public void agregar(ManejadorContacto mContacto){
		try{
			System.out.println("---> USUARIO CONECTADO.GETNOMBRE() "+mContacto.getUsuarioConectado().getNombre());
			Conexion cnx2 = new Conexion();
			ResultSet id = cnx2.ejecutarConsulta("SELECT idUsuario FROM Usuario WHERE nombre='"+mContacto.getUsuarioConectado().getNombre()+"'");
			if(id != null){
				Conexion cn = new Conexion();
				while(id.next()){
					cn.ejecutarSentencia("INSERT INTO Contactos (idUsuario,nombre,apellido,telefotno) VALUES ("+id.getInt("idUsuario")+",'Nombre','Apellido','Telefono')");
					cn = new Conexion();
					cnx2 = new Conexion();
				}
			}
			Conexion x = new Conexion();
			ResultSet res = x.ejecutarConsulta("SELECT idUsuario, idContacto, nombre, apellido, telefotno FROM Contactos WHERE nombre='Nombre'");
				if(res != null){
					while(res.next()){
						System.out.println("NEXT "+res.next());
						Contacto contact2 = new Contacto(res.getInt("idUsuario"),res.getInt("idContacto"),res.getString("nombre"),res.getString("apellido"),res.getString("telefotno"));
						mContacto.getListaDeContactos().add(contact2);
						x = new Conexion();
					}
				}	
			
		}catch(SQLException s){
			s.printStackTrace();
		}
	}

	Boolean ver=false;
	public void handle(Event event){
		if(event instanceof KeyEvent){
			if(((KeyEvent)event).getCode() == KeyCode.ENTER){
				if(verificarDatos()){
					Stage dialogo = new Stage();
					if(mUsuario.autenticar(tfNombre.getText(), pfContra.getText())){
						dialogo.setScene(new Scene(new Label("Bienvenido "+tfNombre.getText())));
						mContacto.setUsuarioConectado(mUsuario.getUsuarioAutenticado());
						tpPrincipal.getTabs().add(tbContactos);
						tpPrincipal.getTabs().remove(tbLogin);
					}else{
						dialogo.setScene(new Scene(new Label("Verifique sus credenciales :(")));
					}
					dialogo.show();
				}
			}
		}
		if(event instanceof MouseEvent){
			if(tvContactos.getSelectionModel().getSelectedItem()!=null){
				if(event.getEventType()==MouseEvent.MOUSE_CLICKED){
					if(((Button)event.getSource()).getText().equalsIgnoreCase(btn1.getText())){
						eliminar(mContacto);
/*						for(Contacto contact : obtenerListaContactos){
							if(cont==contact){
								
							}
						}*/
					}
				}
				
			}
			if(event.getEventType()==MouseEvent.MOUSE_CLICKED){
					if(((Button)event.getSource()).getText().equalsIgnoreCase(btn2.getText())){
						System.out.println("esto es event.getSource "+event.getSource());
						agregar(mContacto);
					}
				}
		}
		if(event instanceof CellEditEvent){
		}
		if(event instanceof ActionEvent){
		}
	}
	public void setMUsuario(ManejadorUsuario mUsuario){
		this.mUsuario = mUsuario;
	}
}
