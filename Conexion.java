package org.david.db;

import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Conexion{
	private static Conexion instancia;
	private Connection cnx;
	private Statement stn;
	public Conexion(){
		try{
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

			cnx = DriverManager.getConnection("jdbc:sqlserver://127.0.0.1\\DOER;databasename=dbContactos2", "guate", "2014");
			stn = cnx.createStatement();
			
		}catch(SQLException sql){
			sql.printStackTrace();
		}catch(ClassNotFoundException cl){
			System.out.println("Driver no encontrado");
		}
	}
	public void ejecutarSentencia(String sentencia){
		try{
			stn.execute(sentencia);
		}catch(SQLException sq){
			sq.printStackTrace();
		}
	}
	public ResultSet ejecutarConsulta(String consulta){
		ResultSet resultado = null;
		try{
			resultado = stn.executeQuery(consulta);
		}catch(SQLException sq){
			sq.printStackTrace();
		}
		return resultado;
	}
	
}
