package org.david.beans;

import javafx.beans.property.StringProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;

public class Contacto{
	private StringProperty nombre, apellido, telefono;
	private IntegerProperty idContacto, idUsuario;
	public Contacto(){
		this.init();
	}
	public Contacto(int idUsuario,int idContacto, String nombre, String apellido, String telefono){
		this.init();
		//System.out.println("CONTRUCTOR CONTACTO "+telefono);
		this.setNombre(nombre);	
		this.setApellido(apellido);
		this.setTelefono(telefono);
		this.setIdUsuario(idUsuario);
		this.setIdContacto(idContacto);
	}
	private void init(){
		this.nombre = new SimpleStringProperty();
		this.apellido = new SimpleStringProperty();
		this.telefono = new SimpleStringProperty();
		this.idUsuario = new SimpleIntegerProperty();
		this.idContacto = new SimpleIntegerProperty();
	}
	
	public String getNombre(){
		return this.nombre.get();
	}
	public void setNombre(String nombre){
		this.nombre.set(nombre);
	}
	public StringProperty nombreProperty(){
		return this.nombre;
	}

	public String getApellido(){
		return this.apellido.get();
	}
	public void setApellido(String apellido){
		this.apellido.set(apellido);
	}
	public StringProperty apellidoProperty(){
		return this.apellido;
	}

	public String getTelefono(){
		return this.telefono.get();
	}
	public void setTelefono(String telefono){
	//	System.out.println("SET TEL "+telefono);
		this.telefono.set(telefono);
	}
	public StringProperty telefonoProperty(){
		return this.telefono;
	}

	public int getIdUsuario(){
		return this.idUsuario.get();
	}
	public void setIdUsuario(int id){
		this.idUsuario.set(id);
	}
	public IntegerProperty idUsuarioProperty(){
		return this.idUsuario;
	}

	public int getIdContacto(){
		return this.idContacto.get();
	}
	public void setIdContacto(int id){
		this.idContacto.set(id);
	}
	public IntegerProperty idContactoProperty(){
		return this.idContacto;
	}
}
