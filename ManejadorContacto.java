package org.david.manejadores;

import javafx.collections.ObservableList;
import javafx.collections.FXCollections;

import java.sql.SQLException;
import java.sql.ResultSet;

import org.david.beans.Contacto;
import org.david.beans.Usuario;
import org.david.db.Conexion;

public class ManejadorContacto{
	private ObservableList<Contacto> listaDeContacto;
	private Conexion cnx;
	private Usuario usuarioConectado;
	public ManejadorContacto(Conexion cnx){
		//FXCollections.observableArrayList(); devuelve una lista observable vacia;
		this.listaDeContacto = FXCollections.observableArrayList();
		this.cnx = cnx;
	}
	public void setUsuarioConectado(Usuario usuarioConectado){
		this.usuarioConectado=usuarioConectado;
		actualizarListaDeContactos();
	}
	public Usuario getUsuarioConectado(){
		return this.usuarioConectado;
	}
	public void actualizarListaDeContactos(){
		if(usuarioConectado!=null){
			ResultSet resultado = cnx.ejecutarConsulta("SELECT idContacto, idUsuario, nombre, apellido, telefotno FROM Contactos WHERE idUsuario="+usuarioConectado.getIdUsuario());
			if(resultado!=null){
				listaDeContacto.clear();
				try{
					while(resultado.next()){
					//	System.out.println("RESULTADO RESULTSET "+resultado.getString("telefotno"));
						Contacto contacto=new Contacto(resultado.getInt("idUsuario"), resultado.getInt("idContacto"), resultado.getString("nombre"), resultado.getString("apellido"), resultado.getString("telefotno"));
						listaDeContacto.add(contacto);
					}
				}catch(SQLException sql){
					sql.printStackTrace();
				}
			}
		}		
	}
	public ObservableList<Contacto> getListaDeContactos(){
		return listaDeContacto;
	}
	
}
