package org.david.beans;

import javafx.beans.property.StringProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;

public class Usuario{
	private StringProperty nombre, apellido, contra;
	private IntegerProperty idUsuario;
	public Usuario(){
		this.init();
	}
	public Usuario(int idUsuario, String nombre, String apellido, String contra){
		this.init();
		this.setNombre(nombre);	
		this.setApellido(apellido);
		this.setContra(contra);
		this.setIdUsuario(idUsuario);
	}
	private void init(){
		this.nombre = new SimpleStringProperty();
		this.apellido = new SimpleStringProperty();
		this.contra = new SimpleStringProperty();
		this.idUsuario = new SimpleIntegerProperty();
	}
	
	public String getNombre(){
		return this.nombre.get();
	}
	public void setNombre(String nombre){
		this.nombre.set(nombre);
	}
	public StringProperty nombreProperty(){
		return this.nombre;
	}

	public String getApellido(){
		return this.apellido.get();
	}
	public void setApellido(String apellido){
		this.apellido.set(apellido);
	}
	public StringProperty apellidoProperty(){
		return this.apellido;
	}

	public String getContra(){
		return this.contra.get();
	}
	public void setContra(String contra){
		this.contra.set(contra);
	}
	public StringProperty contraProperty(){
		return this.contra;
	}

	public int getIdUsuario(){
		return this.idUsuario.get();
	}
	public void setIdUsuario(int id){
		this.idUsuario.set(id);
	}
	public IntegerProperty idUsuarioProperty(){
		return this.idUsuario;
	}
}
